/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package studyplanner;

import Controller.*;

/**
 *
 * @author georgiaamor
 */
public class StudyPlanner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Create the class for the Contoller
        MainController controller = new MainController();
        
        // Start the application
        controller.startApplication();
    }
}
