/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.*;

/**
 *
 * @author georgiaamor
 */
public class LogInController {
    // Create the class for the Model
    private CheckCredentialsModel model;
    
    /**
     * Creates constructor
     * @param model 
     */
    public LogInController(CheckCredentialsModel model) {
        this.model = model;
    }
    
    /**
     * Sets user name
     * @param userName 
     */
    public void setUserName(String userName) {
        model.setUserName(userName = "Georgia");
    }
    
    /**
     * Gets user name
     * @return 
     */
    public String getUserName() {
        return model.getUserName();
    }
    
    /**
     * Sets password
     * @param password 
     */
    public void setPassword(String password) {
        model.setPassword(password = "Hello");
    }
    
    /**
     * Gets password
     * @return 
     */
    public String getPassword() {
        return model.getPassword();
    }
    
    /**
     * Sets error message
     * @param errorMsg 
     */
    public void setErrorMsg(String errorMsg) {
        model.setErrorMsg(errorMsg = "Please try again.");
    }
    
    /**
     * Gets error message
     * @return 
     */
    public String getErrorMsg() {
        return model.getErrorMsg();
    }
}
