/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import View.*;
import Model.*;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import org.xml.sax.SAXException;

/**
 *
 * @author georgiaamor
 */
public class PopulateProfileController {    
    // Create the class for the View
    private NewSemesterProfileView view;
    
    // Create the class for the Model
    private PopulateProfileModel model;
    
    /**
     * Creates constructor
     * @param view
     * @param model 
     */
    public PopulateProfileController(NewSemesterProfileView view, PopulateProfileModel model) {
        this.view = view;
        this.model = model;
    }
    
    /**
     * Sets Semester ID
     * @param semesterID 
     */
    public void setSemesterID(String semesterID) {
        model.setSemesterID(semesterID = view.getSemesterID());
    }
    
    /**
     * Gets Semester ID
     * @return 
     */
    public String getSemesterIdentifier() {
        return view.getSemesterID();
    }
    
    /**
     * Reads xml file containing the semester profile to populate the dashboard
     * 
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException 
     */
    public void getFile() throws FileNotFoundException, IOException, ParserConfigurationException, SAXException {
        File inputXMLFile = new File(view.getFilePath());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputXMLFile);
        doc.getDocumentElement().normalize();
        System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName("Module");
        System.out.println("-------------------");
        
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            
            if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                System.out.println("Module code: " + eElement.getElementsByTagName("ModuleName").item(0).getTextContent());
                System.out.println("Module name: " + eElement.getElementsByTagName("ModuleName").item(0).getTextContent());
                System.out.println("Coursework no.: " + eElement.getElementsByTagName("Coursework").item(0).getTextContent());
                System.out.println("Start Date: " + eElement.getElementsByTagName("StartDate").item(0).getTextContent());
                System.out.println("End Date: " + eElement.getElementsByTagName("EndDate").item(0).getTextContent());
                System.out.println("Examination no.: " + eElement.getElementsByTagName("Examination").item(0).getTextContent());
                System.out.println("Date: " + eElement.getElementsByTagName("Date").item(0).getTextContent());
            }
        }
    }
        
        /**try {
            File xmlFile = new File(view.getFilePath());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            
            doc.getDocumentElement().normalize();
            
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());
            
            NodeList nList = doc.getElementsByTagName("Module");
            
            System.out.println("---------------------");
            
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                
                System.out.println("\nCUrrent Element: " + nNode.getNodeName());
                
                if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Element eElement = (Element) nNode;
                    
                    System.out.println("Module Name: " + eElement.getAttribute("ModuleName"));
                    System.out.println("Coursework: " + eElement.getAttribute("Coursework"));
                    System.out.println("Start Date: " + eElement.getAttribute("StartDate"));
                    System.out.println("End Date: " + eElement.getAttribute("EndDate"));
                    System.out.println("Examination: " + eElement.getAttribute("Examination"));
                    System.out.println("Date: " + eElement.getAttribute("Date"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
}
