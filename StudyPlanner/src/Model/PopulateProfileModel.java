/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author georgiaamor
 */
public class PopulateProfileModel {
    private String semesterID;
    
    /**
     * Creates constructor
     * @param semesterID 
     */
    public PopulateProfileModel(String semesterID) {
        this.semesterID = semesterID;
    }
     
    /**
     * Sets the Semester ID
     * @param semesterID 
     */
    public void setSemesterID(String semesterID) {
        this.semesterID = semesterID;
    }
    
    /**
     * Gets the semester ID
     * @return 
     */
    public String getSemesterID() {
        return semesterID;
    }
}
