/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author georgiaamor
 */
public class CheckCredentialsModel {    
    private String userName;
    private String password;
    private String errorMsg;
 
    /**
     * Creates constructor
     * @param userName
     * @param password
     * @param errorMsg 
     */
    public CheckCredentialsModel(String userName, String password, 
            String errorMsg) {
        this.userName = userName;
        this.password = password;
        this.errorMsg = errorMsg;
    }

    /**
     * Sets the user name
     * @param userName 
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    /**
     * Gets the user name
     * @return 
     */
    public String getUserName() {
        return userName;
    }
    
    /**
     * Sets the password
     * @param password 
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * Gets the password
     * @return 
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Sets the error message
     * @param errorMsg 
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
     
    /**
     * Gets the error message
     * @return 
     */
    public String getErrorMsg() {
        return errorMsg;
    }
}
